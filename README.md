# Tracer un graphique avec Matplotlib

Notebook Jupyter pour s'entrainer à tracer un graphique avec la bibliothèque Matplotlib.pyplot.
On apprendra également comment faire une régression linéaire en utilisant la fonction linregress de la bibliothèque scipy.stats.